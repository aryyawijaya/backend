const express = require('express');

const voterRoomController = require('../controllers/voterroom');

const Router = express.Router();

Router.get('/voter-room', voterRoomController.getVoterByRoom);

module.exports = Router;
