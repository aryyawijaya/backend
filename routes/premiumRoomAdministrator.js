const express = require('express');

const premiumRoomAdminController = require('../controllers/premiumRoomAdministrator');

const Router = express.Router();

Router.post('/premium-room-admin/v2/login', premiumRoomAdminController.postLoginByEmailAndPassword);
Router.post('/premium-room-admin/v2/register', premiumRoomAdminController.postRegister);
Router.get('/premiumRoomAdmin/getAll', premiumRoomAdminController.getAll);
Router.get('/premiumRoomAdmin/getAllRoom',premiumRoomAdminController.getAllRoom);

module.exports = Router;
