const express = require('express');
const uploader = require('../utils/uploader');
// const upload = multer({ dest:'./public/assets/uploads/voterData'});
const roomController = require('../controllers/room');
const { verifyJwt, verifyPremAdmin } = require('../middlewares/auth');
const multer = require('multer');

const upload = multer();
const Router = express.Router();

Router.post('/room/enter', roomController.enterRoom);
Router.post('/room/createPremium', roomController.postPremiumRoom);
Router.post('/room/createFree', roomController.postFreeRoom);
Router.get('/room/getAll', roomController.getAll);
Router.put('/room/editRoom',roomController.editRoom);
Router.post('/room/postVoters',uploader.uploadVoters.single('voters'),roomController.postVoters);
Router.post('/room/postPilihan',uploader.uploadPilihan.single('pilihan'),roomController.postPilihan);
Router.get('/room/getPremiumRooms', roomController.getPremiumRooms);
Router.delete('/room',roomController.deleteRooms);

Router.get('/rooms/v2/premium-room-admin', verifyJwt, verifyPremAdmin, roomController.getPremiumRoomAdminRooms);
Router.get('/rooms/v2', roomController.getRooms);
Router.get('/rooms/v2/:roomId', verifyJwt, roomController.getRoom);
Router.post(
    '/rooms/v2/free/create',
    upload.fields([
        {
            name: 'voters',
            maxCount: 1,
        },
        {
            name: 'pilihan',
            maxCount: 1,
        },
    ]),
    roomController.createFreeRoom,
);
Router.post(
    '/rooms/v2/premium/create',
    verifyJwt,
    verifyPremAdmin,
    upload.fields([
        {
            name: 'voters',
            maxCount: 1,
        },
        {
            name: 'pilihan',
            maxCount: 1,
        },
        {
            name: 'evidence',
            maxCount: 1,
        },
    ]),
    roomController.createPremiumRoom,
);
Router.put('/rooms/v2/:roomId', verifyJwt, verifyPremAdmin, roomController.editRoomV2);
Router.delete('/rooms/v2/:roomId', verifyJwt, verifyPremAdmin, roomController.deleteRoom)

module.exports = Router;
