const express = require('express');

const voterController = require('../controllers/voter');

const Router = express.Router();

Router.post('/voter/loginById', voterController.postLoginById);
Router.post('/voter/loginByEmail', voterController.postLoginByEmail);
Router.get('/voter/getAll',voterController.getAll);

Router.post('/voters/v2/login', voterController.loginV2);

module.exports = Router;
