const db = require('../utils/db');

module.exports = class Vote {
    constructor(voterId, roomId, pilihanId) {
        this.voterId = voterId;
        this.roomId = roomId
        this.pilihanId = pilihanId;
    }

    static findByVotersId(voterId) {
        return db.execute(
            'SELECT * FROM vote WHERE vote.voterId = ?'
            , [voterId]
        );
    }

    static findByRoomId(roomId) {
        return db.execute(
            'SELECT * FROM vote WHERE vote.roomId = ?'
            , [roomId]
        );
    }

    static findByPilihanId(pilihanId) {
        return db.execute(
            'SELECT * FROM vote WHERE vote.pilihanId = ?'
            , [pilihanId]
        );
    }

    static countPilihan(roomId){
        return db.execute(
            'SELECT pr.pilihanId, COUNT(v.voterId) AS freq FROM `pilihanRoom` AS `pr` LEFT JOIN `vote` AS `v` ON pr.pilihanId = v.pilihanId WHERE pr.roomId = ? GROUP BY pr.pilihanId;'
            ,[roomId]
        );
    }

    static createVote(voterId,roomId,pilihanId){
        return db.execute(
            'INSERT INTO `vote` (`voterId`,`roomId`,`pilihanId`) VALUES (?,?,?)'
            ,[voterId,roomId,pilihanId]
        );
    }

    // static findVoterVoteInRoom(voterId,roomId){
    //     return db.execute(
    //         `select * from vote where voterId = ? and roomId = ?;`,
    //         [voterId,roomId]
    //     );
    // }

    static getAll() {
        return db.execute(
            'SELECT * FROM vote'
        );
    }
    
    static getManyVote(roomId) {
        return db.execute(
            `SELECT COUNT(*) AS total FROM vote WHERE vote.roomId = ?`
            , [roomId]
        );
    }

    static async countVoteInRoom(roomId) {
        const [resultSet] = await db.execute(
            'SELECT COUNT(*) as total FROM vote WHERE roomId = ?',
            [roomId]
        );
        return resultSet;
    }

    static async countVotePilihanInRoom(roomId, pilihanId) {
        const [resultSet] = await db.execute(
            'SELECT COUNT(*) as total FROM vote WHERE roomId = ? AND pilihanId = ?',
            [roomId, pilihanId],
        );
        return resultSet;
    }

    static async saveVote(voterId, roomId, pilihanId) {
        const [resultSet] = await db.execute(
            'INSERT INTO `ayobersuara`.`vote` (`voterId`, `roomId`, `pilihanId`) VALUES (?, ?, ?)',
            [voterId, roomId, pilihanId],
        );
        return resultSet;
    }
};
