const db = require('../utils/db');

module.exports = class PilihanRoom {
  constructor(roomId, pilihanId) {
    this.roomId = roomId;
    this.pilihanId = pilihanId;
  }

  static findByPilihanId(pilihanId) {
    return db.execute(
      'SELECT * FROM pilihanRoom WHERE pilihanRoom.pilihanId = ?'
      , [pilihanId]
    );
  }

  static findByRoomId(roomId) {
    return db.execute(
      'SELECT * FROM pilihanRoom WHERE pilihanRoom.roomId = ?'
      , [roomId]
    );
  }

  static getAll() {
    return db.execute(
      'SELECT * FROM pilihanRoom'
    );
  }

  static getPilihanRoom(roomId) {
    return db.execute(
      'SELECT * FROM `pilihanRoom` AS `pr` INNER JOIN `pilihan` AS `pil` ON pr.pilihanId = pil.pilihanId WHERE pr.roomId = ?;'
      , [roomId]
    );
  }

  static async countPilihanInRoom(roomId) {
    const [resultSet] = await db.execute(
      'SELECT COUNT(*) as total FROM pilihanroom WHERE roomId = ?',
      [roomId]
    );
    return resultSet;
  }

  static async findByRoomIdv2(roomId) {
    const [resultSet] = await db.execute(
      'SELECT pilihanId FROM pilihanroom WHERE roomId = ?',
      [roomId],
    );
    return resultSet;
  }

  static async findPilihanRoomv2(roomId) {
    const [resultSet] = await db.execute(
      'SELECT * FROM `pilihanRoom` AS `pr` INNER JOIN `pilihan` AS `pil` ON pr.pilihanId = pil.pilihanId WHERE pr.roomId = ?',
      [roomId],
    );
    return resultSet;
  }

  static async save(roomId, pilihanId) {
    const [resultSet] = await db.execute(
      'INSERT INTO `pilihanroom` (`roomId`, `pilihanId`) VALUES (?, ?)',
      [roomId, pilihanId],
    );
    return resultSet;
  }
}
