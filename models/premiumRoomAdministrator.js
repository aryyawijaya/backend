const db = require('../utils/db');
const RoomAdministrator = require('./roomAdministrator');

module.exports = class PremiumRoomAdministrator extends RoomAdministrator {
  constructor(premiumRoomAdminId, username, email, password) {
    super(premiumRoomAdminId,username);
    this.email = email;
    this.password = password;
  }
  
  static incrementId(id) {
      // Extract the number part of the ID
    let num = parseInt(id.substring(4), 10);

    // Increment the number
    num += 1;

    // Convert the number back to a string and pad it with zeros if necessary
    let numString = num.toString().padStart(6, '0');

    // Return the new ID
    return 'PRA#' + numString;
  }

  static makeWithAttributes(premiumId, username, email, password) {
    return db.execute(
      'INSERT INTO `premiumRoomAdministrator`(`premiumRoomAdminId`, `username`, `email`, `password`) VALUES (?, ?, ?, ?),'
      , [premiumId, username, email, password]
    );
  }

  static findByPremiumId(premiumId) {
    return db.execute(
      'SELECT * FROM room WHERE room.premiumRoomAdminId = ?'
      , [premiumId]
    );
  }

  static async findByEmail(email) {
    const [resultSet] = await db.execute(
      'SELECT * FROM `premiumRoomAdministrator` WHERE `email` = ?'
      , [email]
    );
    return resultSet;
  }

  static async getPremiumRoomAdminIds() {
    const [resultSet] = await db.execute(
      'SELECT premiumRoomAdminId FROM premiumRoomAdministrator ORDER BY premiumRoomAdminId DESC LIMIT 1;'
    );
    return resultSet;
  }

  static async create(premiumRoomAdministrator) {
    const [resultSet] = await db.execute(
      'INSERT INTO `premiumRoomAdministrator`(`premiumRoomAdminId`, `username`, `email`, `password`) VALUES (?, ?, ?, ?)'
      , [premiumRoomAdministrator.premiumRoomAdminId, premiumRoomAdministrator.username, premiumRoomAdministrator.email, premiumRoomAdministrator.password]
    );
    return resultSet;
  }

  static async findByEmailAndPassword(email, password) {
    const [resultSet] = await db.execute(
      'SELECT premiumRoomAdminId, username, email FROM premiumroomadministrator WHERE email = ? AND password = ?',
      [email, password],
    );
    return resultSet;
  }
};
