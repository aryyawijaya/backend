const db = require('../utils/db');

module.exports = class Transaksi {
  constructor(transaksiId, roomId, premiumRoomAdminId, administratorId, expire, price, paymentStatus) {
    this.transaksiId = transaksiId;
    this.roomId = roomId;
    this.premiumRoomAdminId = premiumRoomAdminId;
    this.administratorId = administratorId;
    this.expire = expire;
    this.price = price;
    this.paymentStatus = paymentStatus;
  }
  
  static incrementId(id) {
    // Extract the number part of the ID
    let num = parseInt(id.substring(4), 10);

    // Increment the number
    num += 1;

    // Convert the number back to a string and pad it with zeros if necessary
    let numString = num.toString().padStart(6, '0');

    // Return the new ID
    return 'TRX#' + numString;
  }

  static findByTransaksiId(transaksiId) {
    return db.execute(
      'SELECT * FROM transaksi WHERE transaksi.transaksiId = ?'
      , [transaksiId]
    );
  }

  static findByRoomId(roomId) {
    return db.execute(
      'SELECT * FROM transaksi WHERE transaksi.roomId = ?'
      , [roomId]
    );
}

  static findByPremiumRoomAdminId(premiumId) {
    return db.execute(
      'SELECT * FROM transaksi WHERE transaksi.premiumRoomAdminId = ?'
      , [premiumId]
    );
  }

  static getAll() {
    return db.execute(
      'SELECT * FROM transaksi'
    );
  }

  static uploadEvidence(transaksiId, evidence) {
    return db.execute(
      'UPDATE transaksi SET evidence = ? WHERE transaksiId = ?',
      [evidence, transaksiId]
    );
  }

  static async findLastTransaksiId() {
    const [resultSet] = await db.execute(
      'SELECT transaksiId FROM transaksi ORDER BY transaksiId DESC LIMIT 1',
    );
    return resultSet;
  }

  static async save(transaksiId, roomId, premiumRoomAdminId, administratorId, expire, price, paymentStatus, evidence) {
    const [resultSet] = await db.execute(
      'INSERT INTO `transaksi` (`transaksiId`, `roomId`, `premiumRoomAdminId`, `administratorId`, `expire`, `price`, `paymentStatus`, `evidence`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
      [transaksiId, roomId, premiumRoomAdminId, administratorId, expire, price, paymentStatus, evidence],
    );
    return resultSet;
  }
};
