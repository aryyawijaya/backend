const pilihanRoom = require('../models/pilihanRoom')

module.exports = class PilihanRoomController {
    static getAll = (req, res, next) => {
        pilihanRoom.getAll()
            .then(([result]) => {
                const pilihanRoomData = result;
                return res.status(200).json({ 'message': 'Pilihan Room Data', 'data': pilihanRoomData });
            })
            .catch(err => {
                return res.status(400).json({ 'message': err.message });
            });
    }
    
    static getPilihanRoom = (req, res, next) => {
        pilihanRoom.getPilihanRoom(req.query.roomId)
          .then(([result]) => {
            return res.status(200).json({ 'message': 'All Pilihan in Room', 'data': result });
          })
          .catch(err => {
            return res.status(400).json({ 'message': err.message });
          });
      }
    
}