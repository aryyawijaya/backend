const express = require('express');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
// const multer = require('multer');
const cleaner = require('./utils/data-cleaner');
dotenv.config();

const app = express();
const port = process.env.port;
// const upload = multer();

app.use((req,res,next) => {
  res.setHeader('Access-Control-Allow-Origin','*');
  res.setHeader('Access-Control-Allow-Methods', '*');
  next();
});


//routes
const voterRoutes = require('./routes/voter');
const roomRoutes = require('./routes/room');
const voteRoutes = require('./routes/vote');
const premiumRoutes = require('./routes/premiumRoomAdministrator');
const transaksiRoutes = require('./routes/transaksi');
const pilihanRoutes = require('./routes/pilihan');
const pilihanRoomRoutes = require('./routes/pilihanRoom');
const administratorRoutes = require('./routes/administrator');
const voterRoomRoutes = require('./routes/voterroom');
const freeRoomAdminRoutes = require('./routes/freeRoomAdministrator');

app.get("/", (req,res) => {
  res.status(200).json({ message: "Welcome." });
});

app.use(bodyParser.urlencoded({ extended: false }));

app.use(voterRoutes);
app.use(roomRoutes);
app.use(voteRoutes);
app.use(premiumRoutes);
app.use(transaksiRoutes);
app.use(pilihanRoutes);
app.use(pilihanRoomRoutes);
app.use(administratorRoutes);
app.use(voterRoomRoutes);
app.use(freeRoomAdminRoutes);

app.listen(port, () => {
  console.log(`Now listening on port ${port}...`);
});

module.exports = app;
