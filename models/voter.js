const db = require('../utils/db');

module.exports = class Voter {
  constructor(voterId, name, email, password,otp) {
    this.voterId = voterId;
    this.name = name;
    this.email = email;
    this.password = password;
    this.otp = otp;
  }

  static incrementId(id) {
    // Extract the number part of the ID
    let num = parseInt(id.substring(4), 10);

    // Increment the number
    num += 1;

    // Convert the number back to a string and pad it with zeros if necessary
    let numString = num.toString().padStart(6, '0');

    // Return the new ID
    return 'VTR#' + numString;
  }

  static findById(id) {
    return db.execute(
      'SELECT * FROM voter WHERE voter.voterId = ?'
      , [id]
    );
  }

  static findByEmail(email) {
    return db.execute(
      'SELECT * FROM voter WHERE voter.email = ?'
      , [email]
    );
  }

  static findOtp(voterId){
    return db.execute(
      'SELECT otp FROM voter WHERE voter.voterId =?'
      ,[voterId]
    );
  }

  static getAll() {
    return db.execute(
      'SELECT * FROM voter'
    );
  }

  static async findByVoterIdAndPassword(voterId, password) {
    const [resultSet] = await db.execute(
      'SELECT voterId, name, email FROM voter WHERE voterId = ? AND password = ?',
      [voterId, password],
    );
    return resultSet;
  }

  static async findLastVoterId() {
    const [resultSet] = await db.execute(
      'SELECT voterId FROM voter ORDER BY voterId DESC LIMIT 1',
    );
    return resultSet;
  }

  static async save(voterId, name, email, password) {
    const [resultSet] = await db.execute(
      'INSERT INTO `voter` (`voterId`, `name`, `email`, `password`) VALUES (?, ?, ?, ?)',
      [voterId, name, email, password],
    );
    return resultSet;
  }
};

