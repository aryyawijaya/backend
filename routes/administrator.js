const express = require('express');
const administratorController = require('../controllers/administrator');

const  Router = express.Router(); 

Router.get('/administrator/getAll', administratorController.getAll);
Router.put('/administrator/validateTransaction', administratorController.validateTansaction);

module.exports = Router;
