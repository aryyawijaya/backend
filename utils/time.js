const today = () => {
    const today = new Date();
    today.setHours(today.getHours()+7);
    return today;
}
// setInterval(today, 1000);

const tomorrow = () => {
    const tomorrow = new Date(today());
    tomorrow.setDate(tomorrow.getDate()+1);
    return tomorrow;
}

const nextMonth = () => {
  const nextMonth = new Date(today());
  nextMonth.setMonth(nextMonth.getMonth()+1);
  return nextMonth;
}


console.log('time imported...');
module.exports = {today, tomorrow, nextMonth}
