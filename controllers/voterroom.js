const VoterRoom = require('../models/voterRoom');

module.exports = class VoterRoomController{
  static getVoterByRoom = (req, res, next) => {
    VoterRoom.findVoterByRoomId(req.query.roomId)
      .then(([result]) => {
        return res.status(200).json({ 'message': 'success', 'data': result });
      })
      .catch(err => {
        return res.status(500).json({ 'message': err.message });
      });
  }
}
