const db = require('../utils/db');

module.exports = class Pilihan {
  constructor(pilihanId, name, vision, mision, description, photo) {
    this.pilihanId = pilihanId;
    this.name = name;
    this.vision = vision;
    this.mision = mision;
    this.description = description;
    this.photo = photo;
  }

  static incrementId(id) {
    // Extract the number part of the ID
    let num = parseInt(id.substring(4), 10);

    // Increment the number
    num += 1;

    // Convert the number back to a string and pad it with zeros if necessary
    let numString = num.toString().padStart(6, '0');

    // Return the new ID
    return 'PIL#' + numString;
  }

  static findById(id) {
    return db.execute(
      'SELECT * FROM pilihan WHERE pilihan.pilihanId = ?'
      , [id]
    );
  }

  static getAll() {
    return db.execute(
      'SELECT * FROM pilihan'
    );
  }

  static async findByPilihanIdv2(pilihanId) {
    const [resultSet] = await db.execute(
      'SELECT * FROM pilihan WHERE pilihanId = ?',
      [pilihanId],
    );
    return resultSet;
  }

  static async findLastPilihanId() {
    const [resultSet] = await db.execute(
      'SELECT pilihanId FROM pilihan ORDER BY pilihanId DESC LIMIT 1',
    );
    return resultSet;
  }

  static async save(pilihanId, name, vision, mission, description, photo) {
    const [resultSet] = await db.execute(
      'INSERT INTO `pilihan` (`pilihanId`, `name`, `vision`, `mission`, `description`, `photo`) VALUES (?, ?, ?, ?, ?, ?)',
      [pilihanId, name, vision, mission, description, photo],
    );
    return resultSet;
  }
};
