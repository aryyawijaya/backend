const { verify } = require('jsonwebtoken');
const dotenv = require('dotenv');
const { isEmpty } = require('lodash');

dotenv.config();
const JWT_SECRET_KEY_PREM_ADMIN = process.env.JWT_SECRET_KEY_PREM_ADMIN;
const JWT_SECRET_KEY_VOTER = process.env.JWT_SECRET_KEY_VOTER;
const JWT_SECRET_KEY_FREE_ADMIN = process.env.JWT_SECRET_KEY_FREE_ADMIN;

const isPremAdmin = (token) => {
  try {
    const user = verify(
      token,
      JWT_SECRET_KEY_PREM_ADMIN,
    );
    return { decodedToken: user, err: null };
  } catch (error) {
    return { decodedToken: null, err: error.name };
  }
}

const isVoter = (token) => {
  try {
    const user = verify(
      token,
      JWT_SECRET_KEY_VOTER,
    );
    return { decodedToken: user, err: null };
  } catch (error) {
    return { decodedToken: null, err: error.name };
  }
}

const isFreeAdmin = (token) => {
  try {
    const user = verify(
      token,
      JWT_SECRET_KEY_FREE_ADMIN,
    );
    return { decodedToken: user, err: null };
  } catch (error) {
    return { decodedToken: null, err: error.name };
  }
}

const verifyJwt = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(' ')[1];

  if (!token) {
    return res.status(401).json({
      ok: false,
      message: 'Token not found',
      status: 401,
      data: {}
    });
  }

  req.premAdmin = false;
  req.voter = false;
  req.freeAdmin = false;

  const premAdmin = isPremAdmin(token);
  if (!isEmpty(premAdmin.err)) {
    if (premAdmin.err === 'TokenExpiredError') {
      return res.status(401).json({
        ok: false,
        message: 'Token expired, please login again',
        status: 401,
        data: {}
      });
    }
  } else {
    req.premAdmin = true;
    req.user = premAdmin.decodedToken;
    return next();
  }

  const voter = isVoter(token);
  if (!isEmpty(voter.err)) {
    if (voter.err === 'TokenExpiredError') {
      return res.status(401).json({
        ok: false,
        message: 'Token expired, please login again',
        status: 401,
        data: {}
      });
    }
  } else {
    req.voter = true;
    req.user = voter.decodedToken;
    return next();
  }

  const freeAdmin = isFreeAdmin(token);
  if (!isEmpty(freeAdmin.err)) {
    if (freeAdmin.err === 'TokenExpiredError') {
      return res.status(401).json({
        ok: false,
        message: 'Token expired, please login again',
        status: 401,
        data: {}
      });
    }
  } else {
    req.freeAdmin = true;
    req.user = freeAdmin.decodedToken;
    return next();
  }

  return res.status(403).json({
    ok: false,
    message: 'Invalid token',
    status: 403,
    data: {}
  });
};

// should use after verifyJwt
const verifyPremAdmin = (req, res, next) => {
  if (!req.premAdmin) {
    return res.status(403).json({
      ok: false,
      message: 'Invalid token',
      status: 403,
      data: {},
    });
  }
  next();
}

const verifyVoter = (req, res, next) => {
  if (!req.voter) {
    return res.status(403).json({
      ok: false,
      message: 'Invalid token',
      status: 403,
      data: {},
    });
  }
  next();
}

module.exports = { verifyJwt, verifyPremAdmin, verifyVoter };