const { isEmpty } = require('lodash');
const Voter = require('../models/voter');
const VoterRoom = require('../models/voterRoom');
const jwt = require('jsonwebtoken');

module.exports = class VoterController{
  static postLoginByEmail = (req, res, next) => {
    // get request
    if (!req.query.roomId) {
      return res.status(400).json({ 'message': 'room id in query must be filled!' });
    }

    if (!req.body.email || !req.body.password) {
      return res.status(400).json({ 'message': 'email or password can not be empty' });
    }
    VoterRoom.findByRoomId(req.query.roomId)
      .then(([result]) => {
        const voterRoomData = result[0]

        if (voterRoomData === undefined) {
          return res.status(401).json({ 'message': 'room is not available' });
        }
        Voter.findByEmail(req.body.email)
          .then(([result]) => {
            const voterData = result[0];

            if (voterData === undefined || voterData.voterId != voterRoomData.voterId) {
              return res.status(401).json({ 'message': 'email is not registered' });
            }

            if (voterData.password !== req.body.password) {
              return res.status(401).json({ 'message': 'incorrect password' });
            }

            const voter = new Voter(
              voterData.voterId,
              voterData.name,
              voterData.email,
              voterData.password
            );

            return res.status(200).json({ 'message': 'login with email', 'voter': voter });
          })

          .catch(err => {
            console.log(err);
            return res.status(400).json({ 'message': err.message })
          });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).json({ 'message': err.message })
      });
  };

  static postLoginById = (req, res, next) => {

    // get request
    if (!req.query.roomId) {
      return res.status(400).json({ 'message': 'room id in query must be filled!' });
    }

    if (!req.body.voterId || !req.body.password) {
      return res.status(400).json({ 'message': 'voter Id or password can not be empty' });
    }

    VoterRoom.findByRoomId(req.query.roomId)
      .then(([result]) => {
        const voterRoomData = result
        if (voterRoomData === undefined) {
          return res.status(401).json({ 'message': 'room is not available' });
        }

        Voter.findById(req.body.voterId)
          .then(([result]) => {
            const voterData = result[0];

            let ok = false;
            for (let voter in voterRoomData) {
              if (voterRoomData[voter].voterId === voterData.voterId) {
                ok = true;
                break;
              }
            }
            if (voterData === undefined || !ok) {
              return res.status(401).json({ 'message': 'user Id is not registered' });
            }

            if (voterData.password !== req.body.password) {
              return res.status(401).json({ 'message': 'incorrect password' });
            }

            const voter = new Voter(
              voterData.voterId,
              voterData.name,
              voterData.email,
              voterData.password
            );

            return res.status(200).json({ 'message': 'login with voterId', 'voter': voter });
          })
          .catch(err => {
            console.log(err);
            return res.status(400).json({ 'message': err.message })
          });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).json({ 'message': err.message })
      });
  };

  static getAll = (req, res, next) => {
    Voter.getAll()
      .then(([result]) => {
        const voterData = result;
        return res.status(200).json({ 'message': 'Voter Data', 'data': voterData });
      })
      .catch(err => {
        return res.status(400).json({ 'message': err.message });
      });
  }

  static loginV2 = async (req, res) => {
    const { voterId, password } = req.body;

    if (isEmpty(voterId) || isEmpty(password)) {
      return res.status(400).json({
        ok: false,
        message: 'voterId or password cannot be empty',
        status: 400,
        data: {},
      });
    }

    try {
      const voter = await Voter.findByVoterIdAndPassword(voterId, password);
      if (isEmpty(voter)) {
        return res.status(401).json({
          ok: false,
          message: 'email or password not valid',
          status: 401,
          data: {},
        });
      }

      const token = jwt.sign(voter[0], process.env.JWT_SECRET_KEY_VOTER, { expiresIn: '1h' });
      return res.status(200).json({
        ok: true,
        message: 'login success',
        status: 200,
        data: {
          token: token,
        },
      });
    } catch (error) {
      console.error(`${__filename}. Scope: loginV2. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {}
      });
    }
  }
}