const Room = require('../models/room');
const Voter = require('../models/voter');
const Pilihan = require('../models/pilihan');
const Transaksi = require('../models/transaksi');
const xlsx = require('xlsx');
const { nextMonth } = require('../utils/time');
const PilihanRoom = require('../models/pilihanRoom');
const VoterRoom = require('../models/voterRoom');
const Vote = require('../models/vote');
const { isEmpty } = require('lodash');
const moment = require('moment');
const cloudinary = require('cloudinary').v2;
const DatauriParser = require('datauri/parser');

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

module.exports = class RoomController {
  static enterRoom = (req, res, next) => {
    if (!req.body.roomId && !req.body.password) {
      return res.status(400).json({ 'message': 'Room Id and password field cant be empty' });
    }
    Room.findById(req.body.roomId)
      .then(([result]) => {
        const roomData = result[0];
        if (!roomData.password) {
          return res.status(200).json({ 'message': 'success without password', 'data': roomData });
        }
        if (roomData.password != req.body.password) {
          return res.status(401).json({ 'message': 'password incorrect' });
        }
        return res.status(200).json({ 'message': 'success with password', 'data': roomData });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).json({ 'message': err.message });
      });
  }

  static postPremiumRoom = (req, res, next) => {
    if (!req.query.premiumRoomAdminId || !req.body.name || !req.body || !req.body.password) {
      return res.status(400).json({ 'message': 'premiumRoomAdminId, name, password field cant be empty' });
    }

    Room.getAll()
      .then(([result]) => {
        let lastRoomId = "RMI#000001"

        if (result.length > 0) {
          lastRoomId = result.at(-1).roomId;
          lastRoomId = Room.incrementId(lastRoomId);
        }

        const room = new Room(
          lastRoomId,
          req.query.premiumRoomAdminId,
          req.body.name,
          req.body.password,
          null
        );
        Transaksi.getAll()
          .then(([result])=>{
            let lastTransaksiId = 'TRX#000001';
            
            if (result.length > 0){
              lastTransaksiId = result.at(-1).transaksiId;
              lastTransaksiId = Transaksi.incrementId(lastTransaksiId);
            }

            Room.createPremium(room, lastTransaksiId)
              .then(([result]) => {
                const roomData = result;
                return res.status(200).json({ 'message': 'success create new room', 'data': room, 'transaksiId': lastTransaksiId });
              })
              .catch(err => {
                console.log(err);
                return res.status(400).json({ 'message': err.message });
              });
          })
          .catch(err => {
            console.log(err);
            return res.status(200).json({ 'message': 'success create new room', 'data': room });
          });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).json({ 'message': err.message });
      });
  }

  static postFreeRoom = (req, res, next) => {
    if (!req.body.name || !req.body || !req.body.password) {
      return res.status(400).json({ 'message': 'name, password field cant be empty' });
    }
    const nextMonth_ = nextMonth();
    Room.getAll()
      .then(([result]) => {
        let lastRoomId = "RMI#000001"

        if (result.length > 0) {
          lastRoomId = result.at(-1).roomId;
          lastRoomId = Room.incrementId(lastRoomId);
        }

        const room = new Room(
          lastRoomId,
          null,
          req.body.name,
          req.body.password,
          nextMonth_
        );

        Room.create(room)
          .then(([result]) => {
            const roomData = result;
            return res.status(200).json({ 'message': 'success create new room', 'data': room });
          })
          .catch(err => {
            console.log(err);
            return res.status(400).json({ 'message': err.message });
          });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).json({ 'message': err.message });
      });
  }

  static editRoom = (req, res, next) => {
    if (!req.query.roomId || !req.body.password) {
      return res.status(400).json({ 'message': 'roomId and password cannot be empty' });
    }
    Room.findById(req.query.roomId)
      .then(([result]) => {
        const roomData = result[0];
        if (req.body.password == roomData.password) {
          return res.status(400).json({ 'message': 'password cannot be the same as the previous password' });
        }

        const room = new Room(
          roomData.roomId,
          roomData.premiumRoomAdminId,
          req.body.name,
          req.body.password,
          roomData.expire
        );

        Room.update(room)
          .then(([result]) => {
            return res.status(200).json({ 'message': 'room updated', 'data': room });
          })
          .catch(err => {
            console.log(err);
            return res.status(400).json({ 'err': err.message });
          })
      })
      .catch(err => {
        console.log(err);
        return res.status(400).json({ 'err': err.message });
      })
  }

  static getAll = (req, res, next) => {
    Room.getAll()
      .then(([result]) => {
        const roomData = result;
        return res.status(200).json({ 'message': 'list room', 'data': roomData });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).json({ 'err': err.message });
      })
  }

  static postVoters = (req, res, next) => {
    if (!req.query.roomId) {
      return res.status(400).json({ 'message': 'you are not in a room' });
    }
    let roomId = req.query.roomId;
    if (!req.file) {
      return res.status(400).json({ 'message': 'no file uploaded' });
    }
    const workbook = xlsx.readFile(req.file.path);
    const sheet = workbook.Sheets[workbook.SheetNames[0]];
    const data = xlsx.utils.sheet_to_json(sheet);
    let temp;
    Voter.getAll()
      .then(([result]) => {
        let lastVoterId = "VTR#000001";
        if (result.length != 0) {
          lastVoterId = result.at(-1).voterId;
          lastVoterId = Voter.incrementId(lastVoterId);
        }
        for (let i = 0; i < data.length; i++) {
          temp = data[i];
          Room.addVoter(lastVoterId, temp.name, temp.email, temp.password, roomId)
            .then(([result]) => {
              const logData = {
                "voterId": lastVoterId,
                "name": temp.name,
                "email": temp.email,
                "password": temp.password
              }
              console.log('message:', 'voter Added', logData)
              if (i == data.length - 1) {
                return res.status(200).json({ 'message': 'success', 'data': data });
              }
            })
            .catch((err) => {
              console.log(err);
              return res.status(400).json({ 'message': err.message });
            });
          lastVoterId = Voter.incrementId(lastVoterId);
        }
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).json({ 'message': err.message });
      });
  }

  static postPilihan = (req, res, next) => {
    if (!req.query.roomId) {
      return res.status(400).json({ 'message': 'you are not in a room' });
    }
    let roomId = req.query.roomId;

    if (!req.file) {
      return res.status(400).json({ 'message': 'no file uploaded' });
    }
    const workbook = xlsx.readFile(req.file.path);
    const sheet = workbook.Sheets[workbook.SheetNames[0]];
    const data = xlsx.utils.sheet_to_json(sheet);
    let temp;
    Pilihan.getAll()
      .then(([result]) => {
        let lastPilihanId = "PIL#000001";
        if (result.length != 0) {
          lastPilihanId = result.at(-1).pilihanId;
          lastPilihanId = Pilihan.incrementId(lastPilihanId);
        }
        for (let i = 0; i < data.length; i++) {
          temp = data[i];
          Room.addPilihan(lastPilihanId, temp.name, temp.vision, temp.mission, temp.description, temp.photo, roomId)
            .then(([result]) => {
              const logData = {
                'pilihanId': lastPilihanId,
                'name': temp.name,
                'vision': temp.vision,
                'mission': temp.mission,
                'description': temp.description,
                'photo': temp.photo
              }
              console.log('message:', 'Pilihan added', logData)
              if (i == data.length - 1) {
                return res.status(200).json({ 'message': 'success', 'data': data });
              }
            })
            .catch((err) => {
              console.log(err);
              return res.status(400).json({ 'message': err.message });
            });
          lastPilihanId = Pilihan.incrementId(lastPilihanId);
        }
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).json({ 'message': err.message });
      });
  }

  static getPremiumRooms = (req, res, next) => {
    Room.getPremiumRooms()
      .then(([result]) => {
        return res.status(200).json({ 'message': 'list premium room', 'data': result });
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).json({ 'message': err.message });
      });
  }

  static deleteRooms = (req,res,next) =>{
    if(!req.query.roomId){
      return res.status(400).json({'message' : 'Room id cannot be empty'});
    } 
    Room.deletePremiumRooms(req.query.roomId)  
      .then(([result])=>{
         return res.status(200).json({'message' : 'Room Deleted','data' : result});
       })
      .catch((err)=>{
         console.log(err);
         return res.status(400).json({'message' : err.message})
    });
  }

  static getPremiumRoomAdminRooms = async (req, res) => {
    const { premiumRoomAdminId } = req.user;

    try {
      const rooms = await Room.findByPremiumIdv2(premiumRoomAdminId);
  
      for (let i = 0; i < rooms.length; i++) {
        const room = rooms[i];
  
        const countPilihan = await PilihanRoom.countPilihanInRoom(room.roomId);
        const countVoters = await VoterRoom.countVoterInRoom(room.roomId);
        const countVote = await Vote.countVoteInRoom(room.roomId);
  
        room.countPilihan = countPilihan[0].total;
        room.countVoters = countVoters[0].total;
        room.countVote = countVote[0].total;
      };
      
      res.status(200).json({
        ok: true,
        message: 'Success',
        status: 200,
        data: {
          rooms: rooms,
        }
      });
    } catch (error) {
      console.error(`${__filename}. Scope: getPremiumRoomAdminRooms. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {}
      });
    }
  }

  static getRooms = async (req, res) => {
    try {
      const rooms = await Room.findRooms();

      for (let i = 0; i < rooms.length; i++) {
        const room = rooms[i];
  
        const countPilihan = await PilihanRoom.countPilihanInRoom(room.roomId);
        const countVoters = await VoterRoom.countVoterInRoom(room.roomId);
        const countVote = await Vote.countVoteInRoom(room.roomId);
  
        room.countPilihan = countPilihan[0].total;
        room.countVoters = countVoters[0].total;
        room.countVote = countVote[0].total;
      };

      res.status(200).json({
        ok: true,
        message: 'Success',
        status: 200,
        data: {
          rooms: rooms,
        }
      });
    } catch (error) {
      console.error(`${__filename}. Scope: getRooms. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {}
      });
    }
  }

  static getRoom = async (req, res) => {
    const { roomId } = req.params;
    const isPremAdmin = req.premAdmin;
    const isVoter = req.voter;
    const isFreeAdmin = req.freeAdmin;
    let eligible = false;
    let rooms;

    try {
      rooms = await Room.findByRoomIdv2(roomId);
      if (isPremAdmin) {
        const { premiumRoomAdminId } = req.user;
        eligible = rooms[0].premiumRoomAdminId === premiumRoomAdminId;
      }
      if (isVoter) {
        const { voterId } = req.user;
        const voterRooms = await VoterRoom.findByRoomIdv2(roomId);
        eligible = voterRooms.some((voterRoom) => voterRoom.voterId === voterId);
      }
      if (isFreeAdmin) {
        eligible = req.user.roomId === roomId;
      }
      if (!eligible) {
        return res.status(403).json({
          ok: false,
          message: 'Does not have access to this room',
          status: 403,
          data: {}
        });
      }

      let pilihans = [];
      const pilihanRooms = await PilihanRoom.findByRoomIdv2(roomId);
      const allVotes = await Vote.countVoteInRoom(roomId);
      for (let i = 0; i < pilihanRooms.length; i++) {
        const pilihan = await Pilihan.findByPilihanIdv2(pilihanRooms[i].pilihanId);
        const countPilihanVote = await Vote.countVotePilihanInRoom(roomId, pilihanRooms[i].pilihanId);
        pilihans.push({
          pilihanId: pilihan[0].pilihanId,
          pilihanName: pilihan[0].name,
          photo: pilihan[0].photo,
          percentage: countPilihanVote[0].total / allVotes[0].total || 0,
        });
      }

      const voters = await VoterRoom.findVoterByRoomIdv2(roomId);
  
      const result = {
        room: {
          roomId: rooms[0].roomId,
          name: rooms[0].name,
          expire: rooms[0].expire,
        },
        pilihans: pilihans,
        voters: voters,
      };
      return res.status(200).json({
        ok: true,
        message: 'Success',
        status: 200,
        data: result,
      });
    } catch (error) {
      console.error(`${__filename}. Scope: getRoom. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {}
      });
    }
  }

  static createFreeRoom = async (req, res) => {
    const { name, password } = req.body;
    const votersBuffer = req.files?.voters[0].buffer;
    const pilihanBuffer = req.files?.pilihan[0].buffer;

    if (isEmpty(name) || isEmpty(password) || isEmpty(votersBuffer) || isEmpty(pilihanBuffer)) {
      return res.status(400).json({
        ok: false,
        message: 'name, password, voters file, or pilihan file cannot be empty',
        status: 400,
        data: {},
      });
    }

    try {
      const workbookVoters = xlsx.read(votersBuffer, { type: 'buffer' });
      const sheetVoter = workbookVoters.Sheets[workbookVoters.SheetNames[0]];
      const voters = xlsx.utils.sheet_to_json(sheetVoter);
      
      const workbookPilihan = xlsx.read(pilihanBuffer, { type: 'buffer' });
      const sheetPilihan = workbookPilihan.Sheets[workbookPilihan.SheetNames[0]];
      const pilihan = xlsx.utils.sheet_to_json(sheetPilihan);
      
      const lastRoomId = await Room.findLastRoomId();
      const roomId = isEmpty(lastRoomId) ? 'RMI#000001' : Room.incrementId(lastRoomId[0].roomId);
      const expire = moment(new Date()).add(1, 'month').toDate();
      await Room.save(roomId, null, name, password, expire);

      const lastVoterId = await Voter.findLastVoterId();
      let voterId = isEmpty(lastVoterId) ? 'VTR#000000' : lastVoterId[0].voterId;
      for (let i = 0; i < voters.length; i++) {
        const voter = voters[i];
        voterId = Voter.incrementId(voterId);
        await Voter.save(voterId, voter.name, voter.email, voter.password);
        await VoterRoom.save(voterId, roomId);
      }

      const lastPilihanId = await Pilihan.findLastPilihanId();
      let pilihanId = isEmpty(lastPilihanId) ? 'PIL#000000' : lastPilihanId[0].pilihanId;
      for (let i = 0; i < pilihan.length; i++) {
        const plhn = pilihan[i];
        pilihanId = Pilihan.incrementId(pilihanId);
        await Pilihan.save(pilihanId, plhn.name, plhn.vision, plhn.mission, plhn.description, plhn.photo);
        await PilihanRoom.save(roomId, pilihanId);
      }

      return res.status(201).json({
        ok: true,
        message: 'Free room has been created',
        status: 201,
        data: {},
      });
    } catch (error) {
      console.error(`${__filename}. Scope: createFreeRoom. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {},
      });
    }
  }

  static createPremiumRoom = async (req, res) => {
    const { name, password } = req.body;
    const votersBuffer = !isEmpty(req.files?.voters) ? req.files.voters[0].buffer : null;
    const pilihanBuffer = !isEmpty(req.files?.pilihan) ? req.files.pilihan[0].buffer : null;
    const evidenceBuffer = !isEmpty(req.files?.evidence) ? req.files.evidence[0].buffer : null;
    const evidenceMimetype = !isEmpty(req.files?.evidence) ? req.files.evidence[0].mimetype : null;
    const { premiumRoomAdminId } = req.user;

    if (isEmpty(name) || isEmpty(password) || isEmpty(votersBuffer) || isEmpty(pilihanBuffer) || isEmpty(evidenceBuffer)) {
      return res.status(400).json({
        ok: false,
        message: 'name, password, voters file, pilihan file, or evidence file cannot be empty',
        status: 400,
        data: {},
      });
    }

    try {
      const workbookVoters = xlsx.read(votersBuffer, { type: 'buffer' });
      const sheetVoter = workbookVoters.Sheets[workbookVoters.SheetNames[0]];
      const voters = xlsx.utils.sheet_to_json(sheetVoter);
      
      const workbookPilihan = xlsx.read(pilihanBuffer, { type: 'buffer' });
      const sheetPilihan = workbookPilihan.Sheets[workbookPilihan.SheetNames[0]];
      const pilihan = xlsx.utils.sheet_to_json(sheetPilihan);

      const { uploader } = cloudinary;
      const parser = new DatauriParser();
      const fileFormat = evidenceMimetype.split('/')[1];
      const { base64 } = parser.format(fileFormat, evidenceBuffer);
      const { url } = await uploader.upload(`data:image/${fileFormat};base64,${base64}`);

      const lastRoomId = await Room.findLastRoomId();
      const roomId = isEmpty(lastRoomId) ? 'RMI#000001' : Room.incrementId(lastRoomId[0].roomId);
      await Room.save(roomId, premiumRoomAdminId, name, password, null);

      const lastTransaksiId = await Transaksi.findLastTransaksiId();
      const transaksiId = isEmpty(lastTransaksiId) ? 'TRX#000001' : Transaksi.incrementId(lastTransaksiId[0].transaksiId)
      await Transaksi.save(transaksiId, roomId, premiumRoomAdminId, 'ADM#000001', null, 39999, 0, url);

      const lastVoterId = await Voter.findLastVoterId();
      let voterId = isEmpty(lastVoterId) ? 'VTR#000000' : lastVoterId[0].voterId;
      for (let i = 0; i < voters.length; i++) {
        const voter = voters[i];
        voterId = Voter.incrementId(voterId);
        await Voter.save(voterId, voter.name, voter.email, voter.password);
        await VoterRoom.save(voterId, roomId);
      }

      const lastPilihanId = await Pilihan.findLastPilihanId();
      let pilihanId = isEmpty(lastPilihanId) ? 'PIL#000000' : lastPilihanId[0].pilihanId;
      for (let i = 0; i < pilihan.length; i++) {
        const plhn = pilihan[i];
        pilihanId = Pilihan.incrementId(pilihanId);
        await Pilihan.save(pilihanId, plhn.name, plhn.vision, plhn.mission, plhn.description, plhn.photo);
        await PilihanRoom.save(roomId, pilihanId);
      }

      return res.status(201).json({
        ok: true,
        message: 'Premium room has been created',
        status: 201,
        data: {},
      });
    } catch (error) {
      console.error(`${__filename}. Scope: createPremiumRoom. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {},
      });
    }
  }

  static editRoomV2 = async (req, res) => {
    const { roomId } = req.params;
    const { name, password } = req.body;
    const { premiumRoomAdminId } = req.user;

    if (isEmpty(name) && isEmpty(password)) {
      res.status(400).json({
        ok: false,
        message: 'fill at least name or password field, or both',
        status: 400,
        data: {},
      });
    }

    try {
      const room = await Room.findByRoomIdv2(roomId);
      if (room[0].premiumRoomAdminId != premiumRoomAdminId) {
        return res.status(403).json({
          ok: false,
          message: 'Does not have access to this room',
          status: 403,
          data: {}
        });
      }

      await Room.updateRoom(roomId, name, password);
      return res.status(200).json({
        ok: true,
        message: 'Room has been updated',
        status: 200,
        data: {},
      });
    } catch (error) {
      console.error(`${__filename}. Scope: editRoomV2. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {},
      });
    }
  }

  static deleteRoom = async (req, res) => {
    const { roomId } = req.params;
    const { premiumRoomAdminId } = req.user;

    try {
      const room = await Room.findByRoomIdv2(roomId);
      if (room[0].premiumRoomAdminId != premiumRoomAdminId) {
        return res.status(403).json({
          ok: false,
          message: 'Does not have access to this room',
          status: 403,
          data: {}
        });
      }

      await Room.deletePremiumRooms(roomId);
      return res.status(200).json({
        ok: true,
        message: 'Room has been deleted',
        status: 200,
        data: {},
      });
    } catch (error) {
      console.error(`${__filename}. Scope: deleteRoom. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {},
      });
    }
  }
}
