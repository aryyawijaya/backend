const Transaksi = require('../models/transaksi');

module.exports = class TransaksiController{
  static getAll = (req, res, next) => {
    Transaksi.getAll()
      .then(([result]) => {
        const transaksiData = result;
        return res.status(200).json({ 'message': 'Get all success', 'data': transaksiData });
      })
      .catch(err => {
        return res.status(400).json({ 'message': err.message });
      });
  }

  static uploadEvidence = (req, res, next) => {
    if (!req.query.transaksiId) {
      return res.status(400).json({ 'message': 'query.transaksiId can not be empty' })
    }

    if (!req.file) {
      return res.status(400).json({ 'message': 'file not uploaded' });
    }

    Transaksi.uploadEvidence(req.query.transaksiId, req.file.path)
      .then(([result])=>{
        const uploadData = result;
          return res.status(200).json({ 'message': 'upload evidence success', 'file' : req.file, 'sqlresult' : uploadData });
      }
    )
  }

  static getById = (req, res, next) => {
    if (!req.query.transaksiId){
      return res.status(400).json({ 'message' : 'query.transaksiId cant be empty' });
    }
    Transaksi.findByTransaksiId(req.query.transaksiId)
      .then(([result]) => {
        const transaksiData = result[0];
        if (transaksiData.evidence === null) {
          return res.status(200).json({ 'message': 'Get by id success', 'data': transaksiData});
        }
        transaksiData.evidence = transaksiData.evidence.split("public\\")[1];
        return res.status(200).json({ 'message': 'Get by id success', 'data': transaksiData});
      })
      .catch(err => {
          return res.status(400).json({ 'message': err.message });
      });
  }
}
