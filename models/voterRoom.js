const db = require('../utils/db');

module.exports = class VoterRoom {
    constructor(voterId, roomId) {
        this.voterId = voterId;
        this.roomId = roomId;
    }

    static findByVoterId(voterId) {
        return db.execute(
            'SELECT * FROM voterRoom WHERE voterRoom.voterId = ?'
            , [voterId]
        );
    }

    static findByRoomId(roomId) {
        return db.execute(
            'SELECT * FROM voterRoom WHERE voterRoom.roomId = ?'
            , [roomId]
        );
    }

    static getAll() {
        return db.execute(
            'SELECT * FROM voterRoom'
        );
    }

    static findVoterByRoomId(roomId) {
        return db.execute(
            'SELECT v.voterId, v.name FROM `voterroom` AS `vr` JOIN `voter` AS `v` ON vr.voterId = v.voterId WHERE vr.roomId = ?',
            [roomId]
        );
    }

    static async countVoterInRoom(roomId) {
        const [resultSet] = await db.execute(
          'SELECT COUNT(*) as total FROM voterroom WHERE roomId = ?',
          [roomId]
        );
        return resultSet;
    }

    static async findByRoomIdv2(roomId) {
        const [resultSet] = await db.execute(
            'SELECT voterId FROM voterroom WHERE roomId = ?',
            [roomId],
        );
        return resultSet;
    }

    static async findVoterByRoomIdv2(roomId) {
        const [resultSet] = await db.execute(
            'SELECT v.voterId, v.name FROM `voterroom` AS `vr` JOIN `voter` AS `v` ON vr.voterId = v.voterId WHERE vr.roomId = ?',
            [roomId],
        );
        return resultSet;
    }

    static async save(voterId, roomId) {
        const [resultSet] = await db.execute(
            'INSERT INTO `voterroom` (`voterId`, `roomId`) VALUES (?, ?)',
            [voterId, roomId],
        );
        return resultSet;
    }
};
