const db = require('../utils/db');

module.exports = class Administrator{
    constructor(administratorId,username,password){
        this.administratorId = administratorId;
        this.username = username;
        this.password = password;
    }

  static findById(administratorId){
    return db.execute(
      'SELECT * FROM `administrator` WHERE administrator.administratorId = ?'
      , [administratorId]
    );
  }

  static  findByUsername(username){
    return db.execute(
      'SELECT * FROM `administrator` WHERE administrator.username = ?'
      , [username]
    );
  }

  static  getAll(){
    return db.execute(
      'SELECT * FROM `administrator`'
    );
  }
  
  static validateTansaction(transaksiId, administratorId){
    return db.execute(
      `UPDATE transaksi SET paymentStatus = true, administratorId = ? WHERE transaksiId = ?`,
      [administratorId, transaksiId]
    ); 
  }
}
