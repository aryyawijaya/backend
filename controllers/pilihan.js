const { isEmpty } = require('lodash');
const Pilihan = require('../models/pilihan');
const PilihanRoom = require('../models/pilihanRoom');

module.exports = class PilihanController {
  static getAll = (req, res, next) => {
    Pilihan.getAll()
      .then(([result]) => {
        const pilihanData = result;
        return res.status(200).json({ 'message': 'Pilihan Data', 'data': pilihanData });
      })
      .catch(err => {
        return res.status(400).json({ 'message': err.message });
      });
  }

  static getByRoomId = async (req, res) => {
    const { roomId } = req.params;

    try {
      const pilihan = await PilihanRoom.findPilihanRoomv2(roomId);
      res.status(200).json({
        ok: true,
        message: 'Success',
        status: 200,
        data: pilihan,
      });
    } catch (error) {
      console.error(`${__filename}. Scope: getByRoomId. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {}
      });
    }
  }
}