const db = require("../utils/db");
const schedule = require('node-schedule');
const { today }= require ('./time');

console.log('dataCleaner start working...');
const rule = new schedule.RecurrenceRule();
rule.second = 1;
const job = schedule.scheduleJob(rule, () => {
  db.query(`DELETE FROM room WHERE expire < STR_TO_DATE('${today().toISOString().slice(0, 19)}','%Y-%m-%dT%H:%i:%s')`)
    .then((res) => {
      console.log("dataCleaner running...")
      console.log(res);
    })
    .catch((err) => {
      console.log("error: ", err);
    });
});
