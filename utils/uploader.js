const multer = require('multer');
const path = require('path');
require('dotenv').config();

const storageVoters = multer.diskStorage({
  destination: (req, file, callback) => {
    console.log('destination ok...');
    callback(null,`${process.env.public_asset_dir}/voterData/`);
  },
  filename: (req, file, callback) => {
    const date = new Date();
    let prefix = date.getTime().toString(); 
    console.log('filename ok...');
    callback(null,  prefix + 'voter' + path.extname(file.originalname));
  }
});

const storageEvidence = multer.diskStorage({
  destination: (req, file, callback) => {
    console.log('destination ok...');
    callback(null, `${process.env.public_asset_dir}/evidence/`);
  },
  filename: (req, file, callback) => {
    console.log('filename ok...');
    const date = new Date();
    let prefix = date.getTime().toString();
    callback(null,  prefix + '-evidence' + path.extname(file.originalname));
  }
});

const storagePilihan = multer.diskStorage({
  destination: (req, file, callback) => {
    console.log('destination ok...');
    callback(null,`${process.env.public_asset_dir}/pilihanData/`);
  },
  filename: (req, file, callback) => {
    console.log('filename ok...');
    const date = new Date();
    let prefix = date.getTime().toString(); 
    callback(null,  prefix + 'pilihan' + path.extname(file.originalname));
  }
});


const fileFilterVoters = (req, file, callback) => {
  console.log('fileFilter ok...');
  if(path.extname(file.originalname)==='.xlsx'){
    console.log('correct extname...');
    callback(null, true);
  }else{
    console.log('wrong extname...');
    callback(new Error('unsupported files'), false);
  }
}

const fileFilterEvidence = (req, file, callback) => {
  console.log('fileFilter ok...');
  if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
    console.log('correct extname...');
    callback(null, true);
  }else{
    console.log('wrong extname...');
    callback(new Error('unsupported files'), false);
  }
}

const fileFilterPilihan = (req, file, callback) => {
  console.log('fileFilter ok...');
  if(path.extname(file.originalname)==='.xlsx'){
    console.log('correct extname...');
    callback(null, true);
    console.log('wrong extname...');
  }else{
    callback(new Error('unsupported files'), false);
  }
}

const uploadVoters = multer({
  storage: storageVoters,
  fileFilter: fileFilterVoters
});

const uploadEvidence = multer({
  storage: storageEvidence,
  fileFilter: fileFilterEvidence
});

const uploadPilihan = multer({
  storage: storagePilihan,
  fileFilter: fileFilterPilihan
});

console.log('uploader imported...');
module.exports = { uploadVoters, uploadEvidence, uploadPilihan };
