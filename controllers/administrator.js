const Administrator = require('../models/administrator');

module.exports = class AdministratorController{
  static getAll = (req, res, next) => {
    Administrator.getAll()
      .then(([result]) => {
        const data = result;
        return res.status(200).json({ 'message': 'Administrator Data :', 'data': data });
      })
      .catch(err => {
        return res.status(400).json({ 'message': err.message });
      });
  }

  static validateTansaction = (req, res, next) => {
    if (!req.query.administratorId){
      return res.status(400).json({ 'message': 'you are not authorized' });
    }

    if (!req.body.transaksiId ){
      return res.status(400).json({ 'message': 'transaction id empty' });
    }
    Administrator.validateTansaction(req.body.transaksiId, req.query.administratorId)
      .then(([result]) =>{
      const data = result;
      return res.status(200).json({ 'message': 'Administrator Data :', 'data': data });
      })
      .catch(err => {
        console.log(err)
        return res.status(400).json({ 'message': err.message });
      });
  }
}
