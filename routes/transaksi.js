const express = require('express');
const uploader = require('../utils/uploader');
const transaksiController = require('../controllers/transaksi');

const  Router = express.Router(); 

Router.get('/transaksi/getAll',transaksiController.getAll);
Router.post('/transaksi/uploadEvidence',uploader.uploadEvidence.single('evidence'), transaksiController.uploadEvidence);
Router.get('/transaksi/getById', transaksiController.getById);

module.exports = Router;
