const { isEmpty } = require('lodash');
const Room = require('../models/room');
const jwt = require('jsonwebtoken');

class FreeRoomAdministrator {
  static login = async (req, res) => {
    const { roomId, password } = req.body;
    
    if (isEmpty(roomId) || isEmpty(password)) {
      return res.status(400).json({
        ok: false,
        message: 'roomId or password cannot be empty',
        status: 400,
        data: {},
      });
    }

    try {
      const room = await Room.findByRoomIdAndPassword(roomId, password);
      if (isEmpty(room)) {
        return res.status(401).json({
          ok: false,
          message: 'roomId or password not valid',
          status: 401,
          data: {},
        });
      }

      const token = jwt.sign(room[0], process.env.JWT_SECRET_KEY_FREE_ADMIN, { expiresIn: '1h' });
      return res.status(200).json({
        ok: true,
        message: 'login success',
        status: 200,
        data: {
          token: token,
        },
      });
    } catch (error) {
      console.error(`${__filename}. Scope: login. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {}
      });
    }
  }
}

module.exports = FreeRoomAdministrator;
