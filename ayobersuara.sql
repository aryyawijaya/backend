DROP DATABASE IF EXISTS `ayobersuara`;

CREATE DATABASE `ayobersuara`;

USE `ayobersuara`;

DROP TABLE IF EXISTS `voter`;

CREATE TABLE `voter`(
  `voterId` char(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50),
  `password` varchar(50) NOT NULL,
  `otp` char(4),
  PRIMARY KEY (`voterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `voter` VALUES
('VTR#000001', 'Arga Kusuma Wiratama', 'argakw11@gmail.com','69696',NULL),
('VTR#000002', 'Adinugraha Dharmaputra', NULL ,'12345',NULL);

SELECT * FROM `voter`;

DROP TABLE IF EXISTS `pilihan`;

CREATE TABLE `pilihan`(
  `pilihanId` char(10) primary key not null,
  `name` varchar(50) not null,
  `vision` text,
  `mission` text,
  `description`  text,
  `photo` text
) engine InnoDB, default charset latin1;

INSERT INTO `pilihan`(`pilihanId`, `name`, `vision`, `mission`, `description`, `photo`) values
('PIL#000001','Kanjeng Gusti Ratu Agung Lord Rangga Susuhunan III',null,null,'the king of the king',null),
('PIL#000002','Kanjeng Gusti Ratu Agung Lord Rangga Susuhunan III',null,null,'the king of the king',null);

SELECT * FROM `pilihan`;

DROP TABLE IF EXISTS `premiumRoomAdministrator`;

CREATE TABLE `premiumRoomAdministrator` (
  `premiumRoomAdminId` char(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL UNIQUE,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`premiumRoomAdminId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `premiumRoomAdministrator`(`premiumRoomAdminId`, `username`, `email`, `password`) VALUES 
("PRA#000001", "premAdmin1", "premiumRoom1@gmail.com", "123"),
("PRA#000002", "premAdmin2", "premiumRoom2@gmail.com", "123");

SELECT * FROM `premiumRoomAdministrator`;

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room`(
  `roomId` char(10) NOT NULL,
  `premiumRoomAdminId` char(10) NULL,
  `name` varchar(30),
  `password` VARCHAR(30),
  `expire` date,
  PRIMARY KEY (`roomId`),
  KEY `premiumRoomAdminId` (`premiumRoomAdminId`),
  FOREIGN KEY(`premiumRoomAdminId`) REFERENCES `premiumRoomAdministrator`(`premiumRoomAdminId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `room` (`roomId`, `premiumRoomAdminId`, `name`, `password`, `expire`) VALUES
("RMI#000001", "PRA#000001", "room1", "123", null),
("RMI#000002", "PRA#000002", "room2", "123", null);

SELECT * FROM `room`;

CREATE TABLE `voterRoom`(
  `voterId` char(10) NOT NULL,
  `roomId` char(10) NOT NULL,
  PRIMARY KEY (`voterId`, `roomId`),
  FOREIGN KEY (`voterId`) REFERENCES `voter`(`voterId`),
  FOREIGN KEY (`roomId`) REFERENCES `room`(`roomId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `voterRoom`(`voterId`, `roomId`) VALUES 
('VTR#000001', 'RMI#000001'),
('VTR#000002', 'RMI#000002');

SELECT * FROM `voterRoom`;

DROP TABLE IF EXISTS `administrator`;

CREATE TABLE `administrator` (
  `administratorId` char(10) NOT NULL,
  `username` varchar(50),
  `password` varchar(50),
  PRIMARY KEY (`administratorId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `administrator`(`administratorId`, `username`, `password`) VALUES
("ADM#000001", "admin1", "123"),
("ADM#000002", "admin2", "123");

SELECT * FROM `administrator`;

DROP TABLE IF EXISTS `pilihanRoom`;

CREATE TABLE `pilihanRoom` (
  `roomId` char(10) NOT NULL,
  `pilihanId` char(10) NOT NULL,
  PRIMARY KEY (`roomId`, `pilihanId`),
  KEY `roomId` (`roomId`),
  KEY `pilihanId` (`pilihanId`),
  FOREIGN KEY (`roomId`) REFERENCES `room` (`roomId`),
  FOREIGN KEY (`pilihanId`) REFERENCES `pilihan` (`pilihanId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `pilihanRoom`(`roomId`, `pilihanId`) VALUES 
("RMI#000001", "PIL#000001"),
("RMI#000002", "PIL#000002");

SELECT * FROM `pilihanRoom`;

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `transaksiId` char(10) NOT NULL,
  `roomId` char(10) NOT NULL,
  `premiumRoomAdminId` char(10) NOT NULL,
  `administratorId` char(10) ,
  `expire` date,
  `price` int,
  `paymentStatus` boolean,
  `evidence` text,
  PRIMARY KEY (`transaksiId`),
  KEY `roomid` (`roomId`),
  KEY `premiumRoomAdminId` (`premiumRoomAdminId`),
  KEY `administratorId` (`administratorId`),
  FOREIGN KEY (`roomId`) REFERENCES `room` (`roomId`),
  FOREIGN KEY (`premiumRoomAdminId`) REFERENCES `premiumRoomAdministrator` (`premiumRoomAdminId`),
  FOREIGN KEY (`administratorId`) REFERENCES `administrator` (`administratorId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `transaksi` (`transaksiId`, `roomId`, `premiumRoomAdminId`, `administratorId`, `expire`, `price`, `paymentStatus`) VALUES
("TRX#000001", "RMI#000001", "PRA#000001", null, "2022-12-22", 100000, 0),
("TRX#000002", "RMI#000002", "PRA#000002", "ADM#000001", "2022-12-22", 50000, 1);

SELECT * FROM `transaksi`;

DROP TABLE IF EXISTS `vote`;

CREATE TABLE `vote`(
  `voterId` char(10) NOT NULL,
  `roomId` char(10) NOT NULL,
  `pilihanId` char(10) NOT NULL,
  PRIMARY KEY (`voterId`, `roomId`),
  FOREIGN KEY (`voterId`) REFERENCES `voter`(`voterId`),
  FOREIGN KEY (`roomId`) REFERENCES `room`(`roomId`),
  FOREIGN KEY (`pilihanId`) REFERENCES `pilihan`(`pilihanId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `vote`(`voterId`, `roomId`, `pilihanId`) VALUES 
('VTR#000001', 'RMI#000001', 'PIL#000001');
