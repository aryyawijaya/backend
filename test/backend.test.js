const index = require('../index');
const request = require('supertest');

beforeAll(() => {
  jest.spyOn(console, 'log').mockImplementation(jest.fn());
  jest.spyOn(console, 'debug').mockImplementation(jest.fn());
  jest.spyOn(console, 'warn').mockImplementation(jest.fn());
});

describe('Test index.js', () => {
  test('test GET /' , async () => {
    const res = await request(index)
      .get('/');
  expect(res.status).toEqual(200);
  });
});


describe('Test voter endpoints', () => {
  describe('Test /voter/loginById', () => {
    test('should create new login by id post', async () => {
      const res = await request(index)
        .post('/voter/loginById')
        .query({roomId: 'RMI#000002'})
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          voterId: 'VTR#000002',
          password: '12345'
      });
      expect(res.statusCode).toEqual(200);
    });

    test('should fail when room id is not registered', async () => {
      const res = await request(index)
        .post('/voter/loginById')
        .query({roomId: 'RMI#000000'})
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          voterId: 'VTR#000002',
          password: '12345'
      });
      expect(res.statusCode).toEqual(401);
    });

    test('should fail when voter not registered', async () => {
      const res = await request(index)
        .post('/voter/loginById')
        .query({roomId: 'RMI#000002'})
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          voterId: 'VTR#000001',
          password: '12345'
      });
      expect(res.statusCode).toEqual(401);
    });

    test('should fail when wrong password', async () => {
      const res = await request(index)
        .post('/voter/loginById')
        .query({roomId: 'RMI#000002'})
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          voterId: 'VTR#000002',
          password: '12341'
      });
      expect(res.statusCode).toEqual(401);
    });
  });

  describe('Test /voter/loginByEmail', () => {
    test('should create new login by id post', async () => {
      const res = await request(index)
        .post('/voter/loginByEmail')
        .query({roomId: 'RMI#000001'})
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          email: 'argakw11@gmail.com',
          password: '69696'
      });
      expect(res.statusCode).toEqual(200);
    });

    test('should fail when room id is not registered', async () => {
      const res = await request(index)
        .post('/voter/loginByEmail')
        .query({roomId: 'RMI#000000'})
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          email: 'argakw11@gmail.com',
          password: '69696'
      });
      expect(res.statusCode).toEqual(401);
    });

    test('should fail when voter not registered', async () => {
      const res = await request(index)
        .post('/voter/loginByEmail')
        .query({roomId: 'RMI#000001'})
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          email: 'unknown@gmail.com',
          password: 'ndak tau'
      });
      expect(res.statusCode).toEqual(401);
    });

    test('should fail when wrong password', async () => {
      const res = await request(index)
        .post('/voter/loginByEmail')
        .query({roomId: 'RMI#000001'})
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          email: 'argakw11@gmail,com',
          password: '12345'
      });
      expect(res.statusCode).toEqual(401);
    });
  });
  describe('Test /voter/getAll', () => {
    test('should get all voters', async () => {
      const res = await request(index)
        .get('/voter/getAll')
      expect(res.statusCode).toEqual(200);
    });
  });

});


describe('Test premiumRoomAdmin endpoints', () => {
  describe('Test /premiumRoomAdmin/create', () => {
    test('should create new premiumRoomAdmin', async () => {
      const res = await request(index)
        .post('/premiumRoomAdmin/create')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          username: 'newPremium',
          email: 'newpremiumemail@gmail.com',
          password: '12345'
      });
      expect(res.statusCode).toEqual(200);
    });

    test('should fail when email is already registered', async () => {
      const res = await request(index)
        .post('/premiumRoomAdmin/create')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          username: 'new premium room admin',
          email: 'premiumRoom1@gmail.com',
          password: '12345'
        });
      expect(res.statusCode).toEqual(400);
    });
  });

  describe('Test /premiumRoomAdmin/login', () => {
    test('should return valid premiumRoomAdmin data', async () => {
      const res = await request(index)
        .post('/premiumRoomAdmin/login')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          email: 'premiumRoom1@gmail.com',
          password: '123'
      });
      expect(res.statusCode).toEqual(200);
    });

    test('should fail when password wrong', async () => {
      const res = await request(index)
        .post('/premiumRoomAdmin/login')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          email: 'premiumRoom1@gmail.com',
          password: '12'
        });
      expect(res.statusCode).toEqual(401);
    });

    test('should fail when email is not registered', async () => {
      const res = await request(index)
        .post('/premiumRoomAdmin/login')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          email: 'premiumRoom4@gmail.com',
          password: '123'
        });
      expect(res.statusCode).toEqual(401);
    });
  });

  describe('Test /premiumRoomAdmin/getAll', () => {
    test('should get all premiumRoomAdmin', async () => {
      const res = await request(index)
        .get('/premiumRoomAdmin/getAll')
      expect(res.statusCode).toEqual(200);
    });
  });

  describe('Test /premiumRoomAdmin/getAllRoom', () => {
    test('should get all room', async () => {
      const res = await request(index)
        .get('/premiumRoomAdmin/getAllRoom')
      .query({'premiumRoomAdminId': 'PRA#000001'})
      expect(res.statusCode).toEqual(200);
    });
  });
});

describe('Test vote endpoints', () => {
  describe('Test /vote/create', () => {
    test('should create new premiumRoomAdmin', async () => {
      const res = await request(index)
        .post('/vote/create')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          voterId: 'VTR#000002',
          roomId: 'RMI#000002',
          pilihanId: 'PIL#000002'
      });
      expect(res.statusCode).toEqual(200);
    });

    test('should fail when vote is already made', async () => {
      const res = await request(index)
        .post('/vote/create')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          voterId: 'VTR#000001',
          roomId: 'RMI#000001',
          pilihanId: 'PIL#000001'
        });
      expect(res.statusCode).toEqual(400);
    });
  });

  describe('Test /vote/count endpoint', () => {
    test('should return count of vote in room', async () => {
      const res = await request(index)
        .get('/vote/count')
        .query({ 'roomId' : 'RMI#000001' });
      expect(res.statusCode).toEqual(200);
    });
  });

  describe('Test /vote/getAll', () => {
    test('should get all vote', async () => {
      const res = await request(index)
        .get('/vote/getAll')
      expect(res.statusCode).toEqual(200);
    });
  });

  describe('Test /vote/counts endpoint', () => {
    test('should return counts of vote in room', async () => {
      const res = await request(index)
        .get('/vote/counts')
        .query({ 'roomId' : 'RMI#000001' });
      expect(res.statusCode).toEqual(200);
    });
  });
});


describe('Test administrator endpoints', () => {
  describe('Test /administrator/getAll', () => {
    test('should get all administrator', async () => {
      const res = await request(index)
        .get('/administrator/getAll')
      expect(res.statusCode).toEqual(200);
    });
  });
});


describe('Test pilihan endpoints', () => {
  describe('Test /pilihan/getAll', () => {
    test('should get all administrator', async () => {
      const res = await request(index)
        .get('/pilihan/getAll')
      expect(res.statusCode).toEqual(200);
    });
  });
});


describe('Test pilihanRoom endpoints', () => {
  describe('Test /pilihanRoom/getAll', () => {
    test('should get all administrator', async () => {
      const res = await request(index)
        .get('/pilihanRoom/getAll')
      expect(res.statusCode).toEqual(200);
    });
  });

  describe('Test /pilihanRoom/getPilihanRoom', () => {
    test('should create new premiumRoomAdmin', async () => {
      const res = await request(index)
        .get('/pilihanRoom/getPilihanRoom')
        .query({'roomId': 'RMI#000001'})
      expect(res.statusCode).toEqual(200);
    });
  });
});


describe('Test transaksi endpoints', () => {
  describe('Test /transaksi/getAll', () => {
    test('should get all administrator', async () => {
      const res = await request(index)
        .get('/transaksi/getAll')
      expect(res.statusCode).toEqual(200);
    });
  });
});

describe('Test Room endpoints', () => {
  describe('Test /room/enter', () => {
    test('should authenticate and give room info Room', async () => {
      const res = await request(index)
        .post('/room/enter')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          roomId: 'RMI#000001',
          password: '123'
        });
      expect(res.statusCode).toEqual(200);
    });
  });
  describe('Test /room/createPremium', () => {
    test('should create new premiumRoom', async () => {
      const res = await request(index)
        .post('/room/createPremium')
        .query({ 'premiumRoomAdminId': 'PRA#000001' })
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          name: 'new room',
          password: '12345'
        });
      expect(res.statusCode).toEqual(200);
    });

    test('should fail if premium id is not registered', async () => {
      const res = await request(index)
        .post('/room/createPremium')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          name: 'new room',
          password: '12345'
        });
      expect(res.statusCode).toEqual(400);
    });
  });

  describe('Test /room/createFree', () => {
    test('should create new free room', async () => {
      const res = await request(index)
        .post('/room/createFree')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          name: 'new room',
          password: '12345'
        });
      expect(res.statusCode).toEqual(200);
    });
  });

  describe('Test /room/getAll', () => {
    test('should get all room', async () => {
      const res = await request(index)
        .get('/room/getAll')
      expect(res.statusCode).toEqual(200);
    });
  });

  describe('Test /room/editRoom', () => {
    test('should edit room data', async () => {
      const res = await request(index)
        .put('/room/editRoom')
        .query({ 'roomId' : 'RMI#000002' })
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          name: 'edited room',
          password: '12345'
        });
      expect(res.statusCode).toEqual(200);
    });

    test('should fail to edit room data when roomId is not provided', async () => {
      const res = await request(index)
        .put('/room/editRoom')
        .set({'Content-Type': 'application/x-www-form-urlencoded'})
        .send({
          name: 'edited room',
          password: '12345'
        });
      expect(res.statusCode).toEqual(400);
    });
  });
});
