const { Router } = require('express');
const FreeRoomAdministrator = require('../controllers/freeRoomAdministrator');

const router = Router();

router.post('/free-room-admin/v2/login', FreeRoomAdministrator.login);

module.exports = router;
