const express = require('express');

const PilihanController = require('../controllers/pilihan');

const Router = express.Router();

Router.get('/pilihan/getAll',PilihanController.getAll);

Router.get('/pilihan/v2/:roomId', PilihanController.getByRoomId);

module.exports = Router;