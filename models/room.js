const { isEmpty } = require('lodash');
const db = require('../utils/db');

module.exports = class Room {
  constructor(roomId, premiumRoomAdminId, name, password, expireDate) {
    this.roomId = roomId;
    this.premiumRoomAdminId = premiumRoomAdminId;
    this.name = name;
    this.password = password;
    this.expireDate = expireDate;
  }

  static incrementId(id) {
    // Extract the number part of the ID
    let num = parseInt(id.substring(4), 10);

    // Increment the number
    num += 1;

    // Convert the number back to a string and pad it with zeros if necessary
    let numString = num.toString().padStart(6, '0');

    // Return the new ID
    return 'RMI#' + numString;
  }

  static makeWithAttributes(roomId, premiumId, name, password, expireDate) {
    return db.execute(
      'INSERT INTO `room` (`roomId`, `premiumRoomAdminId`, `name`, `password`, `expire`) VALUES (?,?,?,?,?)'
      , [roomId, premiumId, name, password, expireDate]
    );
  }

  static findById(roomId) {
    return db.execute(
      'SELECT * FROM room WHERE room.roomId = ?'
      , [roomId]
    );
  }

  static findByPremiumId(premiumId) {
    return db.execute(
      'SELECT * FROM room WHERE room.premiumRoomAdminId = ?'
      , [premiumId]
    );
  }

  static getAll() {
    return db.execute(
      'SELECT * FROM room'
    );
  }

  static create(room) {
    return db.execute(
      'INSERT INTO room (`roomId`, `premiumRoomAdminId`, `name`, `password`, `expire`) VALUES (?,?,?,?,?)'
      , [room.roomId, room.premiumRoomAdminId, room.name, room.password, room.expireDate]
    );
  }
  
  static createPremium(room, transaksiId) {
    return db.query(
      // `INSERT INTO room VALUES (?,?,?,?,?);`
      // , [room.roomId, room.premiumRoomAdminId, room.name, room.password, room.expireDate]
      `INSERT INTO room VALUES (?,?,?,?,?);
      INSERT INTO transaksi VALUES (?, ?, ?, null,null,39999,false,null);`
      , [room.roomId, room.premiumRoomAdminId, room.name, room.password, room.expireDate, transaksiId,room.roomId,room.premiumRoomAdminId]
    );
  }
  
  static update(room){
    return db.execute(
      'UPDATE `room` SET `name` = ?, `password` = ? WHERE `roomId` = ?'
      , [room.name, room.password, room.roomId]      
    );
  }

  static addVoter(voterId,name,email,password,roomId){
    return db.query(`
        INSERT INTO voter VALUES (?,?,?,?,null);
        INSERT INTO voterRoom VALUES (?, ?);`
      ,[voterId, name, email, password, voterId, roomId]
    );
  }

  static addPilihan(pilihanId, name, vision, mission, description, photo,roomId){
    return db.query(`
        INSERT INTO pilihan values (?,?,?,?,?,?);
        insert into pilihanRoom values (?, ?);`
      ,[pilihanId, name, vision, mission, description, photo, roomId, pilihanId]
    );
  }

  static getPremiumRooms() {
    return db.execute(
      `SELECT room.roomId, room.name, transaksi.transaksiId, transaksi.paymentStatus FROM room INNER JOIN transaksi ON room.roomId = transaksi.roomId WHERE room.premiumRoomAdminId is not null;`
    );
  }

  static async deletePremiumRooms(roomId){
    await db.execute("DELETE FROM vote WHERE roomId = ?", [roomId])
    await db.execute("DELETE FROM transaksi WHERE roomId = ?", [roomId])
    await db.execute("DELETE FROM pilihanRoom WHERE roomId = ?", [roomId])
    await db.execute("DELETE FROM voterRoom WHERE roomId = ?", [roomId])
    return db.execute("DELETE FROM room WHERE roomId = ?", [roomId])
  }

  static async findByPremiumIdv2(premiumId) {
    const [resultSet] = await db.execute(
      'SELECT roomId, name, expire FROM room WHERE room.premiumRoomAdminId = ?',
      [premiumId]
    );
    return resultSet;
  }

  static async findRooms() {
    const [resultSet] = await db.execute(
      'SELECT roomId, name, expire FROM room'
    );
    return resultSet;
  }

  static async findByRoomIdv2(roomId) {
    const [resultSet] = await db.execute(
      'SELECT * FROM room WHERE roomId = ?',
      [roomId],
    );
    return resultSet;
  }

  static async findByRoomIdAndPassword(roomId, password) {
    const [resultSet] = await db.execute(
      'SELECT roomId, name FROM room WHERE roomId = ? AND password = ? AND expire is not null',
      [roomId, password],
    );
    return resultSet;
  }

  static async findLastRoomId() {
    const [resultSet] = await db.execute(
      'SELECT roomId FROM room ORDER BY roomId DESC LIMIT 1',
    );
    return resultSet;
  }

  static async save(roomId, premiumRoomAdminId, name, password, expire) {
    const [resultSet] = await db.execute(
      'INSERT INTO `room` (`roomId`, `premiumRoomAdminId`, `name`, `password`, `expire`) VALUES (?, ?, ?, ?, ?)',
      [roomId, premiumRoomAdminId, name, password, expire],
    );
    return resultSet;
  }

  static async updateRoom(roomId, name, password) {
    let updateSet = '';
    const values = [];
    if (!isEmpty(name)) {
      updateSet += 'name = ?';
      values.push(name);
    }
    if (!isEmpty(password)) {
        updateSet += !isEmpty(name) ? ', password = ?' : 'password = ?';
        values.push(password);
    }
    values.push(roomId);
    const [resultSet] = await db.execute(
      `UPDATE room SET ${updateSet} WHERE (roomId = ?)`,
      values,
    );
    return resultSet;
  }
};
