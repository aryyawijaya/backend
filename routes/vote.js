const express = require('express');

const VoteController = require('../controllers/vote');
const { verifyJwt, verifyVoter } = require('../middlewares/auth');

const Router = express.Router();

Router.post('/vote/create',VoteController.postVote);
Router.get('/vote/count',VoteController.countVote);
// add new endpoint
Router.get('/vote/counts',VoteController.getManyVote);
Router.get('/vote/getAll',VoteController.getAll);

Router.post('/vote/v2', verifyJwt, verifyVoter, VoteController.voteV2)

module.exports = Router;
