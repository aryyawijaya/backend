const { isEmpty } = require('lodash');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

const PremiumRoomAdministrator = require('../models/PremiumRoomAdministrator');
const Room = require('../models/room');

class PremiumRoomAdministratorController {
  static postRegister = async (req, res, next) => {
    const { username, email, password } = req.body;

    if (isEmpty(username) || isEmpty(email) || isEmpty(password)) {
      return res.status(400).json({
        ok: false,
        message: 'username, email or password cannot be empty',
        status: 400,
        data: null,
      });
    }

    try {
      const checkEmail = await PremiumRoomAdministrator.findByEmail(email);
      if (!isEmpty(checkEmail)) {
        return res.status(400).json({
          ok: false,
          message: 'email already exists',
          status: 400,
          data: null,
        });
      }
  
      const premiumRoomAdmins = await PremiumRoomAdministrator.getPremiumRoomAdminIds();
      let lastPremiumRoomAdminId = 'PRA#000001';
      if (!isEmpty(premiumRoomAdmins)) {
        const { premiumRoomAdminId } = premiumRoomAdmins[0];
        lastPremiumRoomAdminId = PremiumRoomAdministrator.incrementId(premiumRoomAdminId);
      }
      const premiumRoomAdmin = {
        premiumRoomAdminId: lastPremiumRoomAdminId,
        username: username,
        email: email,
        password: password,
      };
      await PremiumRoomAdministrator.create(premiumRoomAdmin);
      delete premiumRoomAdmin.password;
      const token = jwt.sign(premiumRoomAdmin, process.env.JWT_SECRET_KEY_PREM_ADMIN, { expiresIn: '1h' });
      return res.status(201).json({
        ok: true,
        message: 'register success',
        status: 201,
        data: {
          token: token,
        }
      });
    } catch (error) {
      console.error(`${__filename} Scope: postRegister. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {}
      });
    }
  }

  static postLoginByEmailAndPassword = async (req, res, next) => {
    const { email, password } = req.body;

    if (!email || !password) {
      return res.status(400).json({
        ok: false,
        message: 'email or password cannot be empty',
        status: 400,
        data: null,
      });
    }
    
    try {
      const result = await PremiumRoomAdministrator.findByEmailAndPassword(email, password);
      const premiumRoomAdmin = result[0];
      if (isEmpty(premiumRoomAdmin)) {
        return res.status(401).json({
          ok: false,
          message: 'email or password not valid',
          status: 401,
          data: null,
        });
      }
  
      const token = jwt.sign(premiumRoomAdmin, process.env.JWT_SECRET_KEY_PREM_ADMIN, { expiresIn: '1h' });
      return res.status(200).json({
        ok: true,
        message: 'login success',
        status: 200,
        data: {
          token: token,
        }
      });
    } catch (error) {
      console.error(`${__filename}. Scope: postLoginByEmailAndPassword. Error: ${error}`);
      res.status(500).json({
        ok: false,
        message: 'something wrong, check your terminal',
        status: 500,
        data: {}
      });
    }
  }

  static getAll = (req, res, next) => {
    PremiumRoomAdministrator.getAll()
      .then(([result]) => {
        const premiumRoomAdminData = result;
        return res.status(200).json({ 'message': 'Premium Admin Data', 'data': premiumRoomAdminData });
      })
      .catch(err => {
        return res.status(400).json({ 'message': err.message });
      });
  }

  static getAllRoom = (req, res, next) => {
    if (!req.query.premiumRoomAdminId) {
      return res.status(400).json({ 'message': 'premiumRoomAdminId cant be empty' })
    }

    Room.findByPremiumId(req.query.premiumRoomAdminId)
      .then(([result]) => {
        return res.status(200).json({ 'message': 'room premium room admin', 'data': result });
      })
      .catch(err => {
        return res.status(400).json({ 'message': err.message });
      });
  }

}

module.exports = PremiumRoomAdministratorController;

