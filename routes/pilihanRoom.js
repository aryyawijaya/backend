const express = require('express');

const pilihanRoomController = require('../controllers/pilihanRoom');

const Router = express.Router();

Router.get('/pilihanRoom/getAll', pilihanRoomController.getAll);
Router.get('/pilihanRoom/getPilihanRoom', pilihanRoomController.getPilihanRoom);

module.exports = Router;