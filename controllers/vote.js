const Vote = require('../models/vote');
const Room = require('../models/room');
const { isEmpty } = require('lodash');

module.exports = class VoteController{
    static postVote = (req, res, next) => {
        if (!req.body.voterId && !req.body.roomId && !req.body.pilihanId) {
            return res.status(400).json({ 'message': 'Blank' });
        }
        // Vote.findVoterVoteInRoom(req.body.voterId,req.body.roomId)
        //     .then(([result])=> {
        Vote.createVote(req.body.voterId, req.body.roomId, req.body.pilihanId)
            .then(([result]) => {
                const voteData = result[0];
                return res.status(200).json({ 'message': 'Vote Created', 'data': voteData });
            })
            .catch(err => {
                console.log(err);
                if (err.code == 'ER_DUP_ENTRY'){
                    return res.status(400).json({ 'status' : false ,'message': 'You only can vote once in each room.' });
                }
                return res.status(400).json({  'status' : true , 'message': err.message });
            });
            // })
            // .catch(err => {
            //     console.log(err);
            //     return res.status(400).json({ 'message': err.message});
            // });
   }


    static countVote = (req, res, next) => {
        if (!req.query.roomId) {
            return res.status(400).json({ 'message': 'The data is not Available' });
        }
        Room.findById(req.query.roomId)
            .then(([result]) => {
                const voteData = result[0];
                if (voteData == undefined) {
                    return res.status(401).json({ 'message': 'Room is not Available' });
                }
                Vote.countPilihan(req.query.roomId)
                    .then(([result]) => {
                        const voteData = result;
                        return res.status(200).json({ 'message': 'Hasil Vote Adalah : ', 'data': voteData });
                    })
                    .catch(err => {
                        console.log(err);
                        return res.status(400).json({ 'message': err.message });
                    });
            })
            .catch(err => {
                console.log(err);
                return res.status(400).json({ 'message': err.message });
            });
    }

    static getAll = (req, res, next) => {
        Vote.getAll()
            .then(([result]) => {
                const voteData = result;
                return res.status(200).json({ 'message': 'Vote Data', 'data': voteData });
            })
            .catch(err => {
                return res.status(400).json({ 'message': err.message });
            });
    }

    static getManyVote = (req, res, next) => {
        Vote.getManyVote(req.query.roomId)
            .then(([result]) => {
                return res.status(200).json({ 'message': 'many vote in room', 'data': result });
            })
            .catch(err => {
                return res.status(400).json({ 'message': err.message });
            });
    }

    static voteV2 = async (req, res) => {
        const { voterId } = req.user;
        const { roomId, pilihanId } = req.body;

        if (isEmpty(roomId) || isEmpty(pilihanId)) {
            return res.status(400).json({
                ok: false,
                message: 'roomId or pilihanId cannot be empty',
                status: 400,
                data: {}
            });
        }

        try {
            await Vote.saveVote(voterId, roomId, pilihanId);
            return res.status(200).json({
                ok: true,
                message: 'Vote has been saved',
                status: 201,
                data: {},
            });
        } catch (error) {
            if (error.code === 'ER_DUP_ENTRY') {
                return res.status(409).json({
                    ok: false,
                    message: 'You already vote in this room',
                    status: 409,
                    data: {},
                });
            }
            console.error(`${__filename}. Scope: voteV2. Error: ${error}`);
            res.status(500).json({
                ok: false,
                message: 'something wrong, check your terminal',
                status: 500,
                data: {},
            });
        }
    }
}
